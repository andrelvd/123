import java.sql.Timestamp;
import java.util.Scanner;
import java.util.TreeMap;

public class CodingChallenge {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String input;
        int mapSize = 0;

        while(true){
            System.out.println("Insert Cache size: ");
            input = in.nextLine();
            try{
                mapSize = Integer.parseInt(input);
                if(mapSize > 0){ //Check if size from input is over 0
                    System.out.println("correct input");
                    break;
                }else{
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e){
                System.out.println("Incorrect size format.");
            }
        }

        TreeMap<Integer, String> Cache = new TreeMap<>(); // Works with all kinds of variables and it's sorted by it's key which is the timestamp

        input = in.nextLine();
        while (!input.equals("ExitCodingChallenge")) {
            if(Cache.size() < mapSize){ //Insert until map is "filled"
                Cache.put(((int) (new Timestamp(System.currentTimeMillis()).getTime())), input);
                System.out.println("ok");
            }else{ //Remove oldest and insert a new one
                Cache.remove(Cache.firstKey());
                Cache.put(((int) (new Timestamp(System.currentTimeMillis()).getTime())), input);
            }

            System.out.println(Cache.values());
            input = in.nextLine();
        }

        System.out.println("Exiting...");
    }
}
